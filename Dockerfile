# development mode
FROM  mhart/alpine-node:12 as builder
WORKDIR /hartree-gql
COPY . ./
RUN yarn install
RUN yarn build

FROM mhart/alpine-node:12
WORKDIR /hartree-gql
COPY --from=builder /hartree-gql ./
RUN yarn install --production=true
EXPOSE 8080
ENTRYPOINT ["yarn", "serve"]
