const { Pool } = require("pg");
import { environmentConfig } from "../utility/config";
import { LOGGER } from "../utility/logger";
export function connect() {

  return new Pool({
    user: environmentConfig.timeseriesDatabase.user,
    host: environmentConfig.timeseriesDatabase.host,
    database: environmentConfig.timeseriesDatabase.database,
    password: environmentConfig.timeseriesDatabase.password,
    port: environmentConfig.timeseriesDatabase.port
  });
}
