/*
 * Read secrets and other sensitive data into the application
 *
 */
import { LOGGER } from "./logger";
import { readFileSync} from "fs";



export function readPathName(secretNameAndPath) {
    try {
        const readOutput: string = readFileSync(secretNameAndPath, "utf8");
        return readOutput.trim();
    } catch (err) {
        if (err.code !== "ENOENT") {
            LOGGER.error(`Unable to read the secret: ${secretNameAndPath}. Err: ${err}`);
        } else {
            LOGGER.error(`Secret unobtainable, check deployed into swarm: ${secretNameAndPath}. Err: ${err}`);
        }
        return "error";
    }
}