# Stack Up GraphQL


- [Getting started](#getting-started)
    - [.env file](#env-file)
- [References](#handy-references)


## Activities

- [Securely store passwords used by the Server](#securely-store-passwords-used-by-the-server)
- [Test our Server with Jest](#test-our-server-with-jest)
- [Develop a React app with ChartJS in no time](#develop-a-react-app-with-chartjs-in-no-time)



## Getting started

Please start by reading the [Stack Up Courseware](https://jimrbannister.gitlab.io/stack-up/#top).

The *Mock the Server*, **Deployments and configurations**, and **Develop a React app with ChartJS** in no time 
activities are not covered here. These are covered elsewhere.

Fork a new project from https://gitlab.com/JimRBannister/stack-up-graphql if you have already done so.

Unlink the fork see your new project settings -> General -> Advanced in GitLab.

Create a stackup projects directory if one doesn't exist. All projects will be held under this sub-directory including stack-up-graphql.

```bash
mkdir stackup
```

Change directory into your project directory.
```bash
cd stackup
```


Clone your new repository using git if the repository is not already held locally.

```bash
git clone <url>
```


Checkout the development branch
```bash
git checkout dev
```

Adapt the tag names below to reflect your requirements if you wish. Please also change the docker-compose yaml in your copy of the stack-up-administration repository to reflect these changes
The prefix, stackup, represents to the repository name, and you may wish to change this to reflect your repository name and naming conventions.
For example, you may wish to store your images in repository such Docker Hub or AWS Elastic Container Registry to reflect the repository name. 

Stack up graphql is written in typescript. Please refer to the 

## Securely store passwords used by the Server

The first step is build a docker image.

```bash
docker build -f Dockerfile.prod -t stackup/hartree-gql .

```
This build will make use of the .env.prod file to obtain environment variables. Please note that the POSTGRES_PASSWORD has been 
specifically excluded, and will be incorporated as part of the deployment process.

Refer to the stack-up-administration project to continue this activity; it provides instructions on how to deploy your build.

## Test our Server with Jest

View the tests in the __test__ directory.

The configuration file can be found under the .jest folder.

Build the test image.
```bash
docker build -t stackup/hartree-gql-test -f Dockerfile.jest .
```

Refer to the stack-up-administration project to continue this activity; it provides instructions on how to deploy your build before returning to write your own test.

Attempt to write a function _in a new file called averageNumber.ts under src/utility_ to compute the average of a set of numbers in an array.  

Add a unit test that tests this function. See [get started with jest](https://jestjs.io/docs/getting-started).
Add this new test under the __tests__ directory.

Build the test image again and return stack-up-administration project to run the updated test image.



## Handy References!


- [Docker](https://www.docker.com/)
- [Docker compose](https://docs.docker.com/compose/)

